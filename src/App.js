import React, { Component } from 'react';
import './App.css';
import './Button/Button.css';
import Bacon from'./Bacon/Bacon';
import BreadBottom from'./BreadBottom/BreadBottom';
import BreadTop from'./BreadTop/BreadTop';
import Burger from'./Burger/Burger';
import Cheese from'./Cheese/Cheese';
import Meat from'./Meat/Meat';
import Salad from'./Salad/Salad';



class App extends Component {
    state = {
        content: {
            salad: { price: 5, amount:0},
            cheese: { price: 20, amount:0},
            meat: { price: 50, amount:0},
            bacon: { price: 30, amount:0},
        },
        currentPrice: 20


    };
    assemblyFoods = (topping)=> {
        let content = {...this.state.content};
        let contentIn = {...this.state.content[topping]};
        contentIn.amount ++;
        content[topping] = contentIn;
        let currentPrice = this.state.currentPrice ;
        currentPrice += contentIn.price;
        this.setState({
            content,
            currentPrice
        })
    };
    delFoods = (topping)=> {
        let content = {...this.state.content};
        let contentIn = {...this.state.content[topping]};
        contentIn.amount--;
        content[topping] = contentIn;
        let currentPrice = this.state.currentPrice;
        currentPrice -= contentIn.price;
        this.setState({
            content,
            currentPrice
        })
    };

        render() {
    return (
      <div className="App">
          <div className="Title">Total price:{this.state.currentPrice + ' ' + 'kgsom'}</div>
          <div className="Block">
              <button className="Button" onClick={() => this.assemblyFoods('salad')}>Add Salad</button>
              <button className="Button" onClick={() => this.assemblyFoods('cheese')}>Add Cheese</button>
              <button className="Button" onClick={() => this.assemblyFoods('meat')}>Add Meat</button>
              <button className="Button" onClick={() => this.assemblyFoods('bacon')}>Add Bacon</button>
          </div>
          <div className="Block">
              <button className="Button" onClick={() => this.delFoods('salad')} disabled={this.state.content.salad.amount === 0?  true : false } >Delete Salad</button>
              <button className="Button" onClick={() => this.delFoods('cheese')} disabled={this.state.content.cheese.amount === 0?  true : false } >Delete Cheese</button>
              <button className="Button" onClick={() => this.delFoods('meat')} disabled={this.state.content.meat.amount === 0?  true : false } >Delete Meat</button>
              <button className="Button" onClick={() => this.delFoods('bacon')} disabled={this.state.content.bacon.amount === 0?  true : false } >Delete Bacon</button>

          </div>
          <Burger ingridients={this.state.content}/>

      </div>
    );
  }
}

     export default App;


